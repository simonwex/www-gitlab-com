image: dev.gitlab.org:5005/gitlab/gitlab-build-images:www-gitlab-com

variables:
  GIT_DEPTH: "10"
  # Speed up middleman
  NO_CONTRACTS: "true"

.install: &install
  bundle install --jobs 4 --path vendor

before_script: [*install]

cache:
  key: "web"
  paths:
    - vendor

stages:
  - prepare
  - build
  - test
  - deploy
  - dast

lint:
  stage: build
  script:
    - bundle exec rake lint
  tags:
    - gitlab-org

eslint:
  stage: build
  script:
    - yarn install
    - yarn run eslint
  tags:
    - gitlab-org

crop_pictures:
  before_script: []
  stage: prepare
  image: jujhars13/docker-imagemagick
  script:
    - bin/crop-team-pictures
  artifacts:
    paths:
      - data/team.yml
      - data/pets.yml
      - source/images/team/
  tags:
    - gitlab-org

rubocop:
  stage: build
  script:
    - bundle exec rubocop
  tags:
    - gitlab-org

rspec:
  stage: build
  script:
    - bundle exec rspec
  tags:
    - gitlab-org

enforce_relative_links:
  stage: build
  image: alpine
  allow_failure: true
  cache: {}
  before_script:
    - apk add --update the_silver_searcher
  script:
    - set +o errexit
    - ag --filename --numbers --break --nogroup --depth -1 --stats --path-to-ignore ./.relative_links_ignore '(?<!`|")https?://about.gitlab.com(?!`|\S*")' ./source && rc="$?" || rc="$?"
    - if [ "$rc" -eq 0 ]; then exit 1; else exit 0; fi
  tags:
    - gitlab-org

check_links:
  before_script: []
  image: coala/base
  stage: build
  script:
    - git fetch --unshallow && git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*" && git fetch origin master
    - git diff --numstat origin/master..$CI_COMMIT_REF_NAME -- | awk '/(.+\.md)|(.+\.haml)/ { print $3 }' > new_files
    - coala --no-config --ci --bears InvalidLinkBear --settings follow_redirects=True --files="$(paste -s -d, new_files)"
  when: manual
  allow_failure: true
  except:
    - master
  tags:
    - gitlab-org

.build_base: &build_base
  before_script:
    - find source/images/team -type f ! -name '*-crop.jpg' -delete
    - *install
  stage: build
  dependencies:
    - crop_pictures
  artifacts:
    expire_in: 7 days
    paths:
      - public/
      - source/images/team/
  tags:
    - gitlab-org

build_branch:
  <<: *build_base
  script:
    - bundle exec rake build
  except:
    - master

build_master:
  <<: *build_base
  variables:
    MIDDLEMAN_ENV: 'production'
  script:
    - bundle exec rake build pdfs
  only:
    - master

codequality:
  stage: test
  image: docker:stable
  allow_failure: true
  before_script: []
  cache: {}
  dependencies: []
  tags: []
  services:
    - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env SOURCE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
  artifacts:
    paths:
      - codeclimate.json

dependency_scanning:
  stage: test
  image: docker:stable
  allow_failure: true
  before_script: []
  cache: {}
  dependencies: []
  tags: []
  services:
    - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env DEP_SCAN_DISABLE_REMOTE_CHECKS="${DEP_SCAN_DISABLE_REMOTE_CHECKS:-false}"
        --volume "$PWD:/code"
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/dependency-scanning:$SP_VERSION" /code
  artifacts:
    paths:
      - gl-dependency-scanning-report.json

dast:
  stage: dast
  only:
    - branches@gitlab-com/www-gitlab-com
  except:
    - master@gitlab-com/www-gitlab-com
  image: registry.gitlab.com/gitlab-org/security-products/zaproxy
  allow_failure: true
  before_script: []
  cache: {}
  dependencies: []
  tags: []
  services: []
  variables:
    DOCKER_DRIVER: overlay2
    DAST_WEB_SITE: http://$CI_COMMIT_REF_SLUG.about.gitlab.com
  script:
    - if [ -z $DAST_WEB_SITE ]; then echo "Please configure DAST_WEB_SITE env variable" && exit 1; fi
    - mkdir /zap/wrk/
    - /zap/zap-baseline.py -J gl-dast-report.json -t $DAST_WEB_SITE || true
    - cp /zap/wrk/gl-dast-report.json .
  artifacts:
    paths:
      - gl-dast-report.json

review:
  stage: deploy
  allow_failure: true
  before_script: []
  cache: {}
  dependencies:
    - build_branch
  variables:
    GIT_STRATEGY: none
  script:
    - rsync -avz --delete public ~/pages/$CI_COMMIT_REF_SLUG
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$CI_COMMIT_REF_SLUG.about.gitlab.com
    on_stop: review_stop
  only:
    - branches@gitlab-com/www-gitlab-com
  except:
    - master@gitlab-com/www-gitlab-com
  tags:
    - deploy
    - review-apps

review_stop:
  stage: deploy
  before_script: []
  artifacts: {}
  cache: {}
  dependencies: []
  variables:
    GIT_STRATEGY: none
  script:
    - rm -rf public ~/pages/$CI_COMMIT_REF_SLUG
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  only:
    - branches@gitlab-com/www-gitlab-com
  except:
    - master@gitlab-com/www-gitlab-com
  tags:
    - deploy
    - review-apps

deploy:
  stage: deploy
  cache: {}
  variables:
    GIT_STRATEGY: none
  dependencies:
    - build_master
  before_script: []
  script:
    - rsync --delete -avz public/ ~/public/
  environment:
    name: production
    url: https://about.gitlab.com
  tags:
    - deploy
  only:
    - master@gitlab-com/www-gitlab-com
