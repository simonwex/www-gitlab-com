---
layout: markdown_page
title: "Functional Group Updates"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Functional Group Updates are regular updates from a group at GitLab.
For examples please see recent dates in [a search for them](/blog/#stq=functional+group+updates&stp=1).

## Make it worthwhile

1. Aim for 5-10 minutes of presentation with a maximum of 12 minutes. After 12 minutes or more excluding questions participants should remind the speaker to stop presenting ('We're over 10 minutes, as stated in the handbook can we take questions now?').
1. Please focus on the discussion during the call, not the slide content.
1. **Do not read the contents of your slides.** We already read what is on them before you even bring them into view. Everyone is able to read the slide content so the value is in the conversation and questions. Leave time for questions, the presentation is there as warmup to get questions, questions will have the things people care about that you overlooked, they are the most important part of the FGU.
1. People will watch less FGU's if is a bigger time commitment, it is great it they end with time to spare.
1. Save time by writing information on the slides, people can read faster than write, many people will not be able to watch the FGU either live or recorded but can read trough the slides.
1. It is fine if a call ends after 5 minutes, we aim for results, not for time spend.
1. People need to be on time for the team call, end no later than :29 and preferably at :25.
1. If there are more questions or a longer conversation mention on what chat channel the conversation can continue.

## Format

1. Please see the above paragraph about making it worthwhile.
1. We'll use Zoom for now, will switch to YouTube Live later.
1. Use this [slide deck](https://docs.google.com/a/gitlab.com/presentation/d/1JYHRhLaO9fMy1Sfr1WDnCPGv6DrlohlpOzs48VvmlQw/edit?usp=sharing) as template to your presentation. Presentations should allow editing (preferred) or commenting from everyone at GitLab.
1. Make sure to add the link of the presentation to the GitLab Availability Calendar invite at least 24 hours _before_ the call takes place. This allows team members to see the presentation, to click links, have [random access](https://twitter.com/paulg/status/838301787086008320) during the presentation,  and to quickly scan the presentation if they missed it. Please also add who the speaker will be to the presentation and the invite. To do this, go to the [GitLab availability calendar](/handbook/tools-and-tips/#gitlab-availability-calendar), find the event, click on `more details` and edit the description. People Ops will ping the appropriate team member at the 24-hour mark if the event hasn't been updated yet.
1. Reduce distractions for yourself and the attendees by:
   - having the presentation open in its own new browser window, and only sharing that window in Zoom.
   - switching off notifications (from Slack, email, etc.). On Mac, in the notification center in the upper right hand corner, click on Notifications, select Do Not Disturb (sometimes you need to scroll up to see the option).
1. All calls are published on YouTube, with the exception of Finance, Sales, Security, Channel, and Partnerships. Every call is reviewed prior to publishing to make sure no internal information is shared externally.
1. Right now everyone at GitLab the company is invited to each call, we'll invite the wider community later.
1. Attendance is optional.
   - Please add the link to your FGU recordings folder in Google Drive to the invite to allow team members to easily review the recording afterwards.
1. The update is also announced on and the recording linked from our team call agenda.
1. Tone should be informal, like explain to a friend what happened in the group last month, it shouldn't require a lot of presentation.
1. You can invite someone in the team to give the update, it doesn't need to be the team lead, but the team lead is responsible that it is given.
1. Calls are 5 times a week 30 minutes before the team call, 8:00 to 8:25am Pacific.
1. Calls are scheduled by an [EA](/roles/people-ops/executive-assistant/).
1. If you need to reschedule, please *switch* your presenting day with another FGU presenter. If you've agreed to switch do the following:
    - Go to the *GitLab Availabiltiy calendar* invite
    - Update the date of your and the other invite to be switched
    - Choose to send an update to the invitees
    - _If prompted_ with the questions to update 1 or all events, choose to only update this event
1. The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos", which is accessible to all users with a GitLab.com e-mail account.
1. Video recordings will be published on our blog so contributors, users, and customers can see it. We're aiming to publish a blog post once a week of that weeks' recordings with the matching slides in one go.
1. People tend to spend between 30 minutes and 1 hour to prepare their update.
1. Slides with a lot of text that can be read on their own with lots of links are appreciated.
1. There are three layers of content in a presentation:
  - Data, this is the contents the slide.
  - Take away, this is the title of the slide, so use: 'migration 10 days ahead of schedule', instead of 'migration schedule estimates', the combined titles of your slides should a good summary.
  - Feelings, the is the verbal and non-verbal communication in the video feed, how you feel about the take away, 'I'm proud of the band for picking up the pace'.

## FGU Training

In this video our CEO, Sid gives our team tips and tricks for their FGU.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/MN3mzvbgwuc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


## Template for the blog post

For instructions on how to create a blog post, please see our [Blog Handbook](/handbook/marketing/blog/#create-a-new-post).

Please copy the code block below and paste it into a blog post with the file name `yyyy-mm-dd-functional-group-updates.html.md`.

```md
---
title: "GitLab's Functional Group Updates - MMM DD-DD" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Name Surname
author_gitlab: gitlab.com-username
author_twitter: twitter-username
categories: Functional Group Updates
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where attendees have access to the presentation content and presenters can either give a quick presentation or jump straight into the agenda and answering questions.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

```

## Uploading and Editing the Videos

- In order to get access to the GitLab YouTube account, follow the instructions in the secure note in 1Password.
- To upload the video, go to YouTube and click the up arrow at the top right corner, next to the GitLab profile picture.
- Change the security level from "public" to either "unlisted" (only those with the link can view) or "private" (only people with access to the GitLab YouTube account can view), so that you can edit the video prior to it being live.
- Under "Basic Info", change the title to follow this pattern: "Functional Group Update XXX-Team Date". Change the description to:

Functional Group Update XXX-Team Date

Presentation - https://example.com [Input your presentation's URL and check if its set to view only for anyone with the link if using Google Slides]

Try GitLab EE FREE for 30 days, no credit card required - /free-trial/

Want to know more about our free GitLab.com? /gitlab-com/

GitLab License FAQ - /products/

Questions?? - /contact/


- After the video is done uploading, click "Video Manager" in the bottom right corner.
- Edit the video to start when the meeting actually starts:
  - Click **Edit** next to the video icon.
  - Click the **Enhancements** tab on the top menu bar.
  - Click **Trim** on the bottom right. Slide the left edge of the bar to a few moments before the presentation begins, and the right edge of the bar to a few moments after the presentation ends. Click **Done**.
- Take a screenshot of the second slide of the presentation to make it as custom thumbnail for your video on YouTube. You can upload your custom thumbnail under the **Info and Settings** tab when you are editing a video.
- After the video is finished being edited, change the security level back to "public".
- Make sure to use correct format for [embedding videos from YouTube](/handbook/product/technical-writing/markdown-guide/#display-videos-from-youtube) into the blog post. Replace the video URL only.

## Schedule

There is a rotating schedule:

Week 1:

- Monday:       Security              - Kathy
- Tuesday:      General               - Sid
- Wednesday:    Sales                 - Chad
- Thursday:     Meltano               - Jacob

Week 2:

- Monday:       Product               - Job
- Tuesday:      Marketing and Sales   - Courtland
- Wednesday:    Infrastructure        - Andrew
- Thursday:     People Ops            - Barbie
- Friday:       Channel               - Michael

Week 3:

- Monday:       Security              - Kathy
- Tuesday:      UX                    - Sarrah
- Wednesday:    Distribution          - Marin
- Thursday:     Support               - Lee

Week 4:

- Monday:       CI                    - Kamil
- Tuesday:      Frontend              - Tim Zallman
- Wednesday:    Monitoring            - Ben
- Thursday:     Engineering           - Eric
- Friday:       Platform              - Douwe

Week 5:

- Monday:       Security             - Kathy
- Tuesday:      Partnerships         - Eliran
- Wednesday:    Quality              - Mek
- Thursday:     UX Research          - Sarah O' Donnell


Week 6:

- Monday:       Customer Success     - Kristen
- Tuesday:      Discussion           - Sean
- Wednesday:    Security Products    - Philippe
- Thursday:     Finance              - Paul
- Friday:       Marketing            - Sid




## Agenda

A possible agenda for the call is:

1. Accomplishments
1. Concerns
1. Stalled/need help
1. Plans
1. Questions in chat
