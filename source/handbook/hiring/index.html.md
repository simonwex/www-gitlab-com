---
layout: markdown_page
title: "Hiring"
---

## Hiring pages

- [Interviewing](/handbook/hiring/interviewing/)
- [Vacancies](/handbook/hiring/vacancies/)
- [Roles](/handbook/hiring/roles)
- [Lever](/handbook/hiring/lever/)
- [Principles](/handbook/hiring/principles/)

## Related to hiring

- [Benefits](/handbook/benefits/)
- [Visas](/handbook/people-operations/visas/)
- [Compensation](/handbook/people-operations/global-compensation/)
- [Jobs](/jobs/)

## On this page
{:.no_toc}

- TOC
{:toc}

## Definitions

Roles and vacancies are different things and can't be used interchangeably.

- A **role** is a permanent item, it is maintained under /roles, its contents is a superset of all vacancies for that role, and it is created with a merge request.
- A [vacancy](/handbook/hiring/vacancies/) is temporary item, posted on Lever, its contents subset of the role, and it is created by copying parts of a role based on an issue.

We don't use the word job to refer to role or vacancy because it is ambiguous.

People at GitLab can be a specialist on one thing and expert in many:

- A [specialization](/team/structure/#specialist) is specific to a role, each team member can have only one, it defined on the relevant role page. A specialist uses the compensation benchmark of the role.
- An [expertise](/team/structure/#expert) is not specific to a a role, each team member can have multiple ones, the expertises a team member has are listed on our team page.

The following parts describe what someone does at GitLab:

1. Level: Senior
1. Role: Developer
1. Specialist: Gitaly specialist
1. Location: EMEA
1. Expert: Reliability, Durability

We use the following terms to refer to a combination of the above:

- Title (level and role, listed on the contract): Senior Developer
- Headline (all parts except expertise, listed on vacancies): Senior Developer, Gitaly specialist, EMEA

Please use the same order as in the examples above, a few notes:

- Level comes before role.
- Specialist comes after role and always includes 'specialist'.
- Location comes after specialization.

## Equal Employment Opportunity

 Diversity is one of GitLab's core [values](/handbook/values) and
 GitLab is dedicated to providing equal employment opportunities (EEO) to all team members
 and candidates for employment without regard to race, color, religion, gender,
 national origin, age, disability, or genetics. One example of how put this into practice
 is through sponsorship of [diversity events](/2016/03/24/sponsorship-update/)

 GitLab complies with all applicable laws governing nondiscrimination in employment. This policy applies to all terms and conditions of employment, including recruiting, hiring, placement, promotion, termination, layoff, recall, transfer,
 leaves of absence, compensation, and training. GitLab expressly prohibits any form of workplace harassment.
 Improper interference with the ability of GitLab’s team members to perform their role duties
 may result in discipline up to and including discharge. If you have any complaints, concerns,
 or suggestions to do better please [contact People Ops](/handbook/people-operations/#reach-peopleops).

## Country Hiring Guidelines

Please go to [country hiring guidelines](/jobs/#country-hiring-guidelines) for more information.
