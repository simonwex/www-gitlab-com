---
Layout: markdown_page
Title: “Creating New Departments”
---

### Creating New Departments
Successfully creating new departments require that various company systems be updated to capture newly defined department structures. Once the need for a new department arises, follow these steps:

1. Create an issue using the *Department Creation Checklist* template.
1. In the issue, add the appropriate team members from each department included in the checklist.
1. Disclose the nature of the new department. Is the new department simply a name change, or there is a structural modification?   
1. Provide all detail necessary to ensure team members are assigned to the newly created departments. 
