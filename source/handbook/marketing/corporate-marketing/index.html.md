---
layout: markdown_page
title: "Corporate Marketing"
---

## Welcome to the Coporate Marketing Handbook
{:.no_toc}

The Corporate Marketing team includes Global Programs, Customer Marketing, Corporate Events and Design. Corporate Marketing is responsible for the stewardship of the GitLab brand the company messaging/positioning. The team is the owner of the Marketing website and oversees the website strategy. Corporate Marketing creates integrated marketing campaigns, executing global corporate campaigns and supporting the field marketing teams in localizing and verticalizing campaigns for in region execution. The team also oversees Public Relations (PR), the customer reference program and customer marketing.
{: .note}
----

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

----

## Design

### Requesting design help

1. Create an issue in the corresponding project repository
    1. For tasks pertaining to [about.gitlab.com](/) create an issue in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues).
    1. For all other marketing related tasks create an issue in the [marketing project](https://gitlab.com/gitlab-com/marketing/general/issues).
1. Add all relevant details, goal(s), purpose, resources, and links in the issue description. Also `@` mention team members who will be involved.
1. Set due date (if possible) — small last-minute requests are OK, we understand, but in order to do our best work let's not make a habit of it.
1. Add the `Design` and `Website Redesign` (if applicable) label(s) to your issue.

### The `Design` label in issue tracker

The `Design` label helps us find and track issues relevant to the Design team. If you create an issue where Design is the primary focus, please use this label.

### Project prioritization

Per the Design team's discretion, the prioritization of design projects will be based on the direct impact on Marketing.

To get a better sense of [marketing project](https://gitlab.com/gitlab-com/marketing/general/issues) prioritization, you can view the [Design Issue Board](https://gitlab.com/gitlab-com/marketing/general/boards/273272). More specifically the `Design - Working On` and `Design - Top Priority` lists.

Design projects within the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues) can be tracked using the [Design](https://gitlab.com/gitlab-com/www-gitlab-com/issues?label_name%5B%5D=Design) label. The prioritization of projects for [about.gitlab.com](/) can be viewed on the [Website Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/349137).

Any design requests that do not fall in line with the goals and objectives of Marketing will be given a lower priority and factored in as time allows.

### Design touchpoints

The Design team has a rather wide reach and plays a big part in almost all marketing efforts. Design touchpoints range from the [GitLab website](/) to print collateral, swag, and business cards. This includes, but certainly not limited to:

#### Web & Digital
{:.no_toc}
- Redesign, updates, and maintenance of the [GitLab website](/)
- Blog post covers & images
- Landing pages (campaigns, webcasts, events, and feature marketing)
- Swag shop (shop design and new swag)
- Presentation decks & assets
- Ad campaigns
- Email template design

#### Field Design & Branding
{:.no_toc}
- Marketing Design System
- Conference booth design
- Banners & signage
- Swag
- Event-specific slide decks
- Event-specific branding (e.g. GitLab World Tour, Team Summits, etc.)
- Business cards
- One-pagers, handouts, and other print collateral
- GitLab [Brand Guidelines](/handbook/marketing/corporate-marketing/design/brand-guidelines/)

#### Content Design
{:.no_toc}
- Promotional videos & animations
- Social media campaign assets
- Webcast collateral & assets
- eBooks
- Whitepapers
- Infographics & diagrams

## Brand Guidelines

To download the GitLab logo (in various formats and file types) check out our [Press page](/press/).

### GitLab Trademark & Logo Guidelines

GitLab is a registered trademark of GitLab, Inc. You are welcome to use the GitLab trademark and logo, subject to the terms of the [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). The most current version of the GitLab logo can be found on our [Press page](/press/).

Under the Creative Commons license, you may use the GitLab trademark and logo so long as you give attribution to GitLab and provide a link to the license. If you make any changes to the logo, you must state so, along with the attribution, and distribute under the same license.

Your use of the GitLab trademark and logo:

- May not be for commercial purposes;
- May not suggest or imply that you or your use of the GitLab trademark or logo is endorsed by GitLab, or create confusion as to whether or not you or your use of the GitLab trademark or logo is endorsed by GitLab; and
- May not suggest or imply or that you are affiliated with GitLab in any way, or create confusion as to whether or not you are affiliated with GitLab in any way.

Examples of improper use of the GitLab trademark and logo:

- The GitLab name may not be used in any root URL, including subdomains such as `gitlab.company.com` or `gitlab.citool.io'.
- The GitLab trademark and/or logo may not be used as the primary or prominent feature on any non-GitLab materials.

### The Tanuki

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [vision](/about/#vision) that everyone can contribute, our [values](/about/#values), and our [open source stewardship](/2016/01/11/being-a-good-open-source-steward/).

### Colors

While the brand is ever-evolving, the GitLab brand currently consists of six primary colors that are used in a wide array of marketing materials.

##### Hex/RGB

![GitLab Hex/RGB Colors](/images/handbook/marketing/corporate-marketing/design/gitlab-hex-rgb-colors.png)

### Typography

The GitLab brand is paired with two typefaces. [Lato](https://fonts.google.com/specimen/Lato) for large display type and headlines, and [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro) for body copy; or main blocks of written content.

### [GitLab Product UX Guide](https://docs.gitlab.com/ee/development/ux_guide/)

The goal of this guide is to provide written standards, principles and in-depth information to design beautiful and effective GitLab features. This is a living document and will be updated and expanded as we iterate.

### [GitLab Product UX Design Pattern Library](https://brand.ai/git-lab/primary-brand/)

We've broken out the GitLab interface into a set of atomic pieces to form this design pattern library. This library allows us to see all of our patterns in one place and to build consistently across our entire product.

### Brand oversight

Occasionally the [old GitLab logo](https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue and we will follow-up to get it updated. Thanks for contributing to our brand integrity!

## Design System

### Brand resources

- [GitLab logos](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-logo) (web RGB & print CMYK)
- [GitLab wordmarks](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-wordmark) (web RGB & print CMYK)
- Print-ready [event one-pagers](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/print/event-one-pagers)
- Color palette and Typography can be found in the [Brand Guidelines](/handbook/marketing/corporate-marketing/marketing-design-system/brand-guidelines/)
- [Authorized Reseller GitLab Virtuoso Badge](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/authorized-resellers/gitlab-virtuoso-badge)

### Asset libraries

#### Icons

- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/line-icons) (.png)
- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/_resources/icons/svg) (.svg)
- [Icon illustrations](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/illustrated-icons) (.png)
- [Software Development Lifecycle](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/software-development-lifecycle/complete-lifecycle-icons/png)

#### Icon patterns

- [GitLab icon pattern](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/illustrations/icon-pattern)

#### Social media

- [Profile assets](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/social-media/profile-assets/png/assets)

### Templates

#### Presentation decks

- [General GitLab deck template](https://docs.google.com/a/gitlab.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing)
- [Functional Group Update template](https://docs.google.com/a/gitlab.com/presentation/d/1JYHRhLaO9fMy1Sfr1WDnCPGv6DrlohlpOzs48VvmlQw/edit?usp=sharing)
- [GitLab pitch deck template](https://docs.google.com/a/gitlab.com/presentation/d/1LC1lT-gxpl1oUZ2InX4Oni9T4MfR0DFF0RLi4uNxBBQ/edit?usp=sharing)

# Editorial style guide

The following guide outlines the set of standards used for all written company
communications to ensure consistency in voice, style, and personality across all
of GitLab's public communications.

## Table of contents

- [About](#about)
  - [GitLab the community](#community)
  - [GitLab the company](#company)
  - [GitLab the product](#product)
- [Tone of voice](#voice)
- [General style guidelines](#guidelines)
- [Abbreviations](#abbreviations)
- [Acronyms](#acronyms)
- [Ampersands](#ampersands)

## About

### GitLab the community

GitLab is an [open source project](https://gitlab.com/gitlab-org/gitlab-ce/)
with a large community of contributors. Over 1,800 people worldwide have
contributed to GitLab's source code.

### GitLab the company

GitLab Inc. is a company based on the GitLab open source project. GitLab Inc. is
an active participant in our community (see our [stewardship of GitLab CE](/stewardship)
for more information), as well as offering GitLab, a product (see below).

### GitLab the product

GitLab is a single applicaiton for the complete DevOps lifecyle. See the 
[product elevator pitch](/handbook/marketing/product-marketing/#elevator-pitch) 
for additional messaging.

## Tone of voice

The tone of voice we use when speaking as GitLab should always be informed by
our [Content Strategy](https://gitlab.com/gitlab-com/marketing/blob/master/content/content-strategy.md#strategy).
Most importantly, we see our audience as co-conspirators, working together to
define and create the next generation of software development practices. The below
table should help to clarify further:


<table class="tg">
  <tr>
    <th class="tg-yw4l">We are:</th>
    <th class="tg-yw4l">We aren't:</th>
  </tr>
  <tr>
    <td class="tg-yw4l">Equals in our community</td>
    <td class="tg-yw4l">Superior</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Knowledgeable</td>
    <td class="tg-yw4l">Know-it-alls</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Empathetic</td>
    <td class="tg-yw4l">Patronizing</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Straightforward</td>
    <td class="tg-yw4l">Verbose</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Irreverent</td>
    <td class="tg-yw4l">Disrespectful</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Playful</td>
    <td class="tg-yw4l">Jokey</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Helpful</td>
    <td class="tg-yw4l">Dictatorial</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Transparent</td>
    <td class="tg-yw4l">Opaque</td>
  </tr>
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
</table>

We explain things in the simplest way possible, using plain, accessible language.

We keep a sense of humor about things, but don't make light of serious issues or
problems our users or customers face.

We use colloquialisms and slang, but sparingly (don't look like you're trying too hard!).

We use [inclusive, gender-neutral language](https://litreactor.com/columns/5-easy-ways-to-make-your-writing-gender-neutral).

## General style guidelines

We use American English by default and follow the AP Style guide unless otherwise
noted in this style guide. We do not use title case.

## Abbreviations



## Acronyms

When using [acryonyms or initialisms](https://www.dailywritingtips.com/acronym-vs-initialism/),
ensure that your first mention uses the full term and includes the shortened
version in parentheses directly afterwards. From then on you
may use the acronym or initialism instead.

> Example: A Contributor License Agreement (CLA) is the industry standard for open
source contributions to other projects.

Below are some common acroynms and initialisms we use which don't need to be
defined first (but use sparingly, see [Tone of voice](#tone-of-voice) above):

- AFAIK - as far as I know
- ICYMI - in case you missed it (for social only)
- IIRC - if I recall correctly
- IRL - in real life
- TL;DR - too long; didn't read

## Ampersands

Use ampersands only where they are part of a company's name or the title of a
publication or similar. Example: Barnes & Noble

Do not use as a substitute for "and."

## Capitalization
Use [sentence case](https://www.thoughtco.com/sentence-case-titles-1691944) for
all titles and headings.

All GitLab feature names must be capitalized. Examples: GitLab Issue Boards,
GitLab Pages.

See [below](#word-list) for styling of specific words.

## Contractions

We favor contractions ("can't," "didn't," "it's," "we're") to sound more human and
less formal.

## Formatting
## Lists
## Numbers

Spell out one to nine. Use numerals for 10 upwards. Try to avoid beginning a
sentence with a number, but if unavoidable, spell it out.

Numbers with four or more digits should include a comma. Examples: 1,800; 100,000

## Punctuation

Only one space after a period is necessary.

Include one space after ellipses (... )

See [below](#word-list) for when to hyphenate specific words.

We use en dashes (–) rather than em dashes (—). Include a space before and after the dash.

## Quotes

Use double quotation marks for direct quotes, and single quotation marks for a quote
within a quote. Single quotation marks may also be used for specialist terms or sayings.

Include punctuation in quotation marks.

> Example: What do you think of the claim, "software is eating the world?"

## Word choice

When in doubt, use the "future" styling of a word. So, "internet" is not capitalized,
"startup" is not hyphenated, etc.

## Word list

How to spell and style commonly used words.

- Agile
   - A is capitalized when referring to [Agile methodology](https://en.wikipedia.org/wiki/Agile_software_development)
- cloud native
    - not capitalized, and no hyphen, regardless of how it is used
- continuous delivery, deployment, integration
    - not capitalized
- developer
    - may only be shortened to "dev" on social
- DevOps
    - D and O always capitalized
- Git
    - always capitalized
- GitHub
- GitLab
    - G and L are always capitalized, even in GitLab.com
- internet
    - not capitalized
- Kubernetes
    - always capitalized, never abbreviated to K8s (except on social)
- open source
    - no hyphen, regardless of how it is used
- remote-only/remote only
    - hyphenated only when appearing before a noun ("GitLab is a remote-only organization"/"GitLab is remote only")
- set up/setup
    - two words for the verb, one for the noun ("How to set up a Kubernetes cluster"/"Let's walk through the Kubernetes cluster setup process")
- sign up/signup
    - two words for the verb, one for the noun ("Sign up for a GitLab.com account"/"Upon signup, you will be sent a confirmation email")
- startup
    - no hyphen

## Appendix A: When to use en dashes and semicolons


## Appendix B: UK vs. American English

Use American spelling in your communications. Please consult [this list of
spelling differences](https://en.oxforddictionaries.com/spelling/british-and-spelling).

----

## Swag

* We aim to have our swag delight and/ or be useful.
* We aim to make limited edition and themed swag for the community to collect. Bigger events will have custom tanuki stickers in small runs, only available at their specific event.
* We aim to do swag in a way that doesn't take a lot of time to execute => self serve => [web shop](https://gitlab.myshopify.com/)
* We get a lot of requests to send swag, and we try to respond to them all. If you would like to get some GitLab swag for your team or happening, please see below for more info on submitting a swag. Note: We recommend you request swag at least 4 weeks out from the event date or we may not be able to accommodate your request.

### Community Swag Requests: 
Email your request to sponsorships@gitlab.com. In your request please include the expected number of guests, the best shipping address, and phone number along with what kind of swag you are hoping for. The swag we have available can be found on our online store. Note: We recommend you request swag at least 4 weeks out from the event date or we may not be able to accommodate your request.

### Internal GitLab swag ordering:
* Event Swag: To request additional GitLab materials for an event you are attending see instructions below.
  * The event in questions must be 4 or more weeks away for all swag and material requests, as rush shipping is not an option using the link below.
  * You can place event swag orders with [this link](https://get.printfection.com/ybddf/6351118868). This link can only be accessed via your gitlab.com email address. The request will be approved on the back end by the Field Marketing team. If you do not see what you are looking for in this portal contact events@gitlab.com.
  * For printed materials (one pager, cheat sheets) please email events@gitlab.com.
  * For larger swag orders (stickers in a quantity of 100 or greater), do not go through the swag store but rather use our [Stickermule](https://www.stickermule.com/) account or ping emily@gitlab.com. You will need the same information (address and order quantity). 
* GitLabbers- if you would like to order something from the GitLab swag shop we have a discount code you can use for 30% off. Please see the swag slack channel to get code to be used in the [store](https://shop.gitlab.com/) at checkout. It can be found in the channel description.

### Swag for customer/ prospects 
Anyone with access to Salesforce can send swag through Salesforce directly. Please review [sending swag to customers paramaters](https://gitlab.com/gitlab-com/sales/issues/144). Instructions on how to do so below:
1. Create general task for whichever contact you want to send swag to. You need to create a task and save it before the "order swag" button will show up as an option in the task menu.
1. Go into created and saved task and click the button that says "order swag".
1. If you do not see the window pop up for ordering, please check your pop up settings and verify pop ups are not being blocked.   
1. Previous step will bring up a printfrection page where you can order desired swag. (please note not all swag is listed in sfdc, for more options email emily@gitlab.com).

* We have GitLab stationary/ note cards- leave note in swag slack channel of you would like a batch to send notes to prospects/ customers/ community members.

* _NOTE:_ Please keep in mind the [list of countries we do not do business in](/handbook/sales-process/images_sales_process/#export-control).

----

## Speakers

##### For GitLabbers Attending Events/ Speaking    

- If you are interested in find out about speaking opportunities join the #CFP slack channel. Deadlines for talks can be found in the slack channel and in the master GitLab [events spreadsheet](https://docs.google.com/spreadsheets/d/16usWToIsD-loDQYpflaMiGTmERMYSieNj_QAuk5HBeY/edit#gid=1939281399).
- If you want help building out a talk, coming up with ideas for a speaking opportunity, or have a customer interested in speaking start an issue in the marketing project using the CFP submissions template and tag any associated event issues. Complete as much info as possible and we will ping you with next steps. We are happy to help in anyway we can, including public speaking coaching.
- If there is an event you would like to attend, are attending, speaking, or have proposed a talk and you would like support from GitLab to attend this event the process goes as follows:
 1. Contact your manager for approval to attend/ speak.
 1. After getting approval from your manager to attend, add your event/ talk to the [events page](/events/) and submit merge request to Emily Kyle.
   1. Your travel and expenses will not be approved until your event/ engagement has been added to the events page. 
   2. If you are speaking please note your talk in the description when you add it to the Events Page. 
   3. If you are not already on the [speakers page](/find-a-speaker/), please add yourself.
 1. We suggest bringing swag and/or stickers with you. See notes on #swag lower on this page for info on ordering event swag.

##### Finding and Suggesting Speakers and Submitting to CFPs   

- Speaker Portal: a catalogue of talks, speaker briefs and speakers can be found on our [Find a Speaker page](/find-a-speaker/). Feel free to add yourself to this page and submit a MR if you want to be in our speaker portal and are interested in being considered for any upcoming speaking opportunities.
- If you have a customer interested in speaking start an issue in the marketing project using the CFP submissions template and tag any associated event issues. Complete as much info as possible and we will ping you with next steps. We are happy to help in anyway we can, including public speaking coaching.

----

## Corporate Events

### Goals
- Brand
    - Drive market awareness and perceptions
    - Thought Leadership
- Evangelism - Talk to as many people as we can about GitLab to drive awareness. Our presence should be friendly, knowledgeable, and genuine. We are all the brand and we want to bring that brand to life and be memorable.
- Hiring - Always be recruiting. See someone doing a great job of evangelism for another product? Ask that person to grab a coffee.
- Partnerships - Organizations adding support for GitLab and/or shipping GitLab with their offering.
