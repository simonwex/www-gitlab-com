---
layout: markdown_page
title: Product categories
---

## Introduction

We want intuitive interfaces both within the company and with the wider community.
This makes it more efficient for everyone to contribute or to get a question answered.
Therefore, the following interfaces are based on the product categories defined on this page:

- [Product Vision](/direction/product-vision/)
- [Direction](/direction/#functional-areas)
- [Software Development Life-Cycle (SDLC)](/sdlc/#stacks)
- [Product Features](/features/)
- [Documentation](https://docs.gitlab.com/ee/#complete-devops-with-gitlab)
- [Backend teams](/handbook/backend/)
- [Product manager responsibilities](/handbook/product/#who-to-talk-to-for-what)
- Our deck, the slides that we use to describe the company
- Product marketing specializations

## Definitions

The functionality of GitLab is ordered in 4 level hierarchy:

1. Stages: One of the 7 DevOps lifecycle stages (see below), one of the two areas that span the entire lifecycle, or a non-lifecycle stage like Auth or BizOps.
1. Categories: High-level capabilities that may be a standalone product at another company. e.g. Portfolio Management.
1. Capabilities: An optional group of functionality that is made up of smaller discrete features. e.g. Epics. Capabilities are listed as the second tier of bullets below. We have [feature categories](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/feature_categories.yml) but it is incomplete and not used everywhere.
1. Features: Small, discrete functionalities. e.g. Issue weights. Some common features are listed within parentheses to facilitate finding responsible PMs by keyword. Features are maintained in [features.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml).

## DevOps lifecycle stages

![DevOps lifecycle](/handbook/product/categories/devops-loop-and-spans.png)

At GitLab the Dev and Ops split is different because our CI/CD functionality is one codebase that falls under Ops.

## Dev

- Product: [Job]
- Backend: [Tommy]
- Product Marketing: [John]

1. Manage - [Jeremy]
    - Reporting & Analytics
        - Cycle Analytics
        - DevOps Score (previously Conversational Development Index / ConvDev Index)
        - [Usage statistics](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) (Version check (incl. version.gitlab.com), Usage ping)
1. Plan - [Victor]
    - Project management
        - Issue tracker (assignees, milestones, time tracking, due dates, labels, weights, quick actions, email notifications, todos, search, Elasticsearch integration, Jira and other third-party issue management integration)
        - Issue boards
    - Portfolio management
        - Epics
        - Roadmaps
    - [Service desk]
    - Chat integration (Mattermost, Slack)
    - Markdown
1. Create - [Victor] and [James] and [Andreas]
    - Source code management
        - Version control / Git repository management (Commits, file locking, LFS, protected branches, project templates, import/export, mirroring, housekeeping (e.g. git gc), hooks) - [James]
        - Gitaly - [James]
        - Snippets - [Andreas]
    - Code review (merge requests, diffs, approvals) - [Victor]
    - Web IDE - [James]
    - Wiki - [Andreas]
    - Admin Area - [Andreas]
1. Auth - [Jeremy]
    - User management & authentication (incl. LDAP, signup, abuse)
    - Groups and [Subgroups]
    - Navigation
    - Audit log
    - GitLab.com (our hosted offering of GitLab)
    - Subscriptions (incl. license.gitlab.com and customers.gitlab.com)
    - [Internationalization](https://docs.gitlab.com/ee/development/i18n/)
1. Gitter - n/a
1. Production - [Job]
1. [Geo] - [Andreas]

## Ops

- Product: [Mark]
- Backend: [Dalia]
- Product Marketing: [William]

1. Verify - [Vivek]
    - [Continuous Integration (CI)]
        - Unit Testing
        - Integration Testing
        - Acceptance Testing
        - Performance Testing
        - GitLab Runner
1. Package - [Josh]
    - Container Registry
    - Binary Repository
1. Release - [Fabio]
    - [Continuous Delivery (CD)]
        - Review apps
    - Release Automation
    - [Pages]
1. Configure - [Daniel]
    - Application Control Panel
        - Auto DevOps
    - Infrastructure Configuration
    - Operations
        - ChatOps
    - Feature Management
        - Feature flags
1. Monitor - [Josh]
    - Application Performance Monitoring (APM)
        - Metrics
        - Tracing
    - Infrastructure Monitoring
        - Cluster Monitoring
    - Production Monitoring
    - Error Tracking
    - Logging
1. Secure - [Fabio]
    - Security Testing
        - Static Application Security Testing (SAST)
        - Dynamic application security testing (DAST)
        - Dependency Scanning
        - Container Scanning
        - License Management
        - Runtime Application Self-Protection (RASP)
1. Distribution - [Josh] (note: the engineering backend team reports into the dev backend department)
    - Omnibus
    - Cloud Native Installation
1. BizOps - [Josh]

## Composed categories

GitLab also does the things below that are composed of multiple categories.

1. Software Composition Analysis (SCA) = Dependency Scanning + License Management
1. Interactive Application Security Testing (IAST) = Dynamic application security testing (DAST) + Runtime Application Self-Protection (RASP)

[Jeremy]: /team/#d3arWatson
[Fabio]: /team/#bikebilly
[Josh]: /team/#joshlambert
[Mark]: /team/#MarkPundsack
[William]: /team/#thewilliamchia
[James]: /team/#jamesramsay
[Job]: /team/#Jobvo
[John]: /team/#j_jeremiah
[Victor]: /team/#victorwu416
[Daniel]: /team/#danielgruesso
[Vivek]: /team/#vivekbhupatiraju
[Tommy]: /team/#tommy.morgan
[Dalia]: /team/#dhavens
[Andreas]: /team/#andreasmarc
[Pages]: /features/pages/
[Geo]: /features/gitlab-geo/
[Continuous Integration (CI)]: /features/gitlab-ci-cd/
[Continuous Delivery (CD)]: /features/gitlab-ci-cd/
[Subgroups]: /features/subgroups/
[Service Desk]: /features/service-desk/
