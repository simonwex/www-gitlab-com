---
layout: markdown_page
title: "Inclusion"
---

![Our Global Team](/images/team-photo-mexico-summit.jpg){: .illustration}*<small>In January 2017, our team of 150 GitLabbers from around the world!</small>*

### GitLab's Commitment to Employee Inclusion and Development

GitLab recognizes that to build a diversity initiative that resonates for our organization, we need to redefine it in a way that embodies our values and reinforces our global workforce. We also know that many [diversity programs fail](https://hbr.org/2016/07/why-diversity-programs-fail). Rather than focusing on building diversity as a collection of activities, data, and metrics, we're choosing [to build and institutionalize](http://www.russellreynolds.com/en/Insights/thought-leadership/Documents/Diversity%20and%20Inclusion%20GameChangers%20FINAL.PDF) a culture that is inclusive and supports all employees equally to achieve their professional goals. We will refer to this intentional culture curation as inclusion and development (i & d).

We have also recorded a training on this subject:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/5bCmjiK8h5w" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Identity data

Please see our [identity data](/culture/inclusion/identity-data)

### 100% remote

We have 100% remote team which means our candidates are not limited by geography – Our door, if you will, is open to everyone and we [champion this approach](http://www.remoteonly.org/), to the extent that it’s possible, for all companies.

### Technical interviews on real issue

Another practice we employ is to ask GitLab candidates to work on real issues as a part of our hiring process. This allows people who may not stand out well on paper - which is what the hiring process traditionally depends on - to really show us what they can do and how they approach issues. For us, that’s proven more valuable than where an candidate studied or has worked before.

### Diversity Sponsorship program

One of our biggest initiatives is the [GitLab Diversity Sponsorship program](/community/sponsorship/). This is a sponsorship opportunity we offer to any “diversity in tech” event globally. We offer funds to help support the event financially and, if the event is in a city we have a GitLab team member, we get hands-on by offering to coach and/or give a talk whenever possible.

### Values

We try to make sure [our values](/handbook/values/) reflect our goal to be inclusive, [one of our top 6 values is diversity](/handbook/values/#diversity).

In addition, the very nature of our company is meant to facilitate inclusion. We believe in asynchronous communication where possible which enables our teammates to have flexibility in the hours and locations from which they work. The no-ask vacation time also contributes to making it easier to combine work with other obligations and activities, whether that be volunteering with inner city youth, helping your kids with homework, going to the gym or a variety of other activities.  

### Double referral bonus

We want to encourage and support diversity on our team and in our hiring practices, so we will offer a [$2000 referral bonus](/handbook/incentives/#referral-bonuses) for hires from underrepresented groups in the tech industry for engineering roles at GitLab. This underrepresented group is defined as: women, African-Americans, Hispanic-Americans/Latinos, and veterans.

### Inclusive benefits

We list our [Transgender Medical Services](/handbook/benefits/#transgender-medical-services) and [Pregnancy & Maternity Care](/handbook/benefits/#pregnancy--maternity-care) publicly so people don't have to ask for them during interviews.

### Inclusive language

In our [general guidelines](/handbook/general-guidelines/) we list: 'Use inclusive language. For example, prefer "Hi everybody" or "Hi people" to "Hi guys".'

### Inclusive interviewing

As part of [our interviewing process](/handbook/hiring/interviewing/) we list: "The candidate should be interviewed by at least one female GitLab team member."

### Training

#### Unconscious bias training

To be truly inclusive is to be aware of your own ingrained biases as well as strategies for blunting the effects of those biases. As part of our inclusivity efforts we recommend everyone to partake in [the Harvard project Implicit test](https://implicit.harvard.edu/implicit/takeatest.html) which focusses in on the hidden causes of everyday discrimination.

#### Salesforce Inclusion Training

[Salesforce Equality at Work Training](https://trailhead.salesforce.com/trails/champion_workplace_equality).
 To earn badges and save your responses, you'll need to sign up! Use your GitLab address to sign in using Google+.  
   * [Business Value of Equality](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_diversity_and_inclusion) (This module has two units. The second is specific to Salesforce values and mission and is not required or suggested for our training.)
   * [Impact of Unconscious Bias](https://trailhead.salesforce.com/en/trails/champion_workplace_equality/modules/workplace_equality_inclusion_challenges)
   * [Diversity and Incusion Strategies](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_be_more_inclusive)
   * [Equality Ally Strategies](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_ally_strategies)

### GitLab Blog Posts
   * [GitLab Offer Sponsorship of $500 for Diversity Events](/2016/02/02/gitlab-diversity-sponsorship/)
   * [Rails Girls Summer of Code 2016](/2016/02/23/rails-girls-summer-of-code-2016/)

### Never done

We recognize that having an inclusive organization is never done. If you work at GitLab please join our #inclusion chat channel. If you don't work for us please email brittany@gitlab.com with suggestions and concerns.
