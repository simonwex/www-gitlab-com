---
layout: markdown_page
title: GitLab for Education
---

GitLab is being widely used by educational institutions around the world.

# Educational Pricing

GitLab’s educational pricing policy applies to self-hosted GitLab only.
Starter, Premium and Ultimate are all applicable under the policy.

GitLab's educational pricing is for educational institutions, As per this model only staff are counted towards the 'user count', students can be added free of charge after a purchase for staff users.

Please [contact our sales team](/sales/) for more information. 