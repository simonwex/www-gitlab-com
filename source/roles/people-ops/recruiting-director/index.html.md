---
layout: job_page
title: "Recruiting Director"
---

## Responsibilities:

* Provide strategic direction for the stellar recruiting team at GitLab.  We are growing and are looking for a strong, hands-on Recruiting leader to support that growth. This will include developing new methods to find passive talent and talent from low cost areas across the world.
* While building and growing your own team, it is also critical to partner with the leaders of the organization to ensure that they are planning for the right hires and are invested in the process of finding, evaluating, and onboarding their talent.
* Identify and Operationalize the technology needs of the recruiting team - identifying needs, determining solutions and integrating tools and systems
* Partner with internal and external teams to deep dive on trends, opportunities and build projects to continuously improve the effectiveness of finding the best talent for GitLab
Actively research and source for high priority, executive positions across the organization.
* Evangelize a leading candidate experience from the initial recruiter interview to candidate onboarding
* Manage the recruitment team workload, evaluate and assign priorities to internal recruiting team emphasizing ownership and accountability
* Manage outside vendors and partners
* Build out GitLab talent programs including diversity recruiting, college/campus recruiting, internships and apprenticeships.
* Utilize Lever ATS to define recruiting metrics and using data to implement improvements for talent acquisition
* Cultivate strong partnerships with hiring teams and executives, and influence change throughout the entire hiring process
* Training for hiring managers and team members on GitLab hiring practices. Also influence those hire practices as they may need to be iterated on to drive the best, scalable results for GitLab.

## About you:

* Strong attention to detail and ability to work well with changing information
* Comfortable using Technology
* Effective and concise verbal and written communication skills with the ability to collaborate with cross functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem solving
* Have implemented various HRIS/ATS systems or recruiting tools while delivering internal OKRs
* 5-10+ years recruiting experience with a minimum of 2 years managing a recruiting team.
* Experience hiring Global Talent in areas like Europe, India and China

## Hiring Process:
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
* Next, candidates will be invited to schedule a 45 minute interview with our Chief Culture Officer
* After that, candidates will be invited to schedule a 30 minute interview with at least one of our People Operations Team members
* Candidate then may be invited to two different 45 minute interviews with two different hiring managers
* Finally, our CEO may choose to conduct a final interview.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
